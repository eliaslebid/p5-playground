'use strict';

import 'p5'

const Sketch = (p) => {
    p.setup = () => {
        p.createCanvas(p.windowWidth, p.windowHeight)
    };

    p.draw = () => {
        p.ellipse(50, 50, 80, 80);
    };

    p.mousePressed = () => {

    };
};

export default Sketch;
